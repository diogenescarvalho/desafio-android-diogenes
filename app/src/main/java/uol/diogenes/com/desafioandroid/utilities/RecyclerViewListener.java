package uol.diogenes.com.desafioandroid.utilities;

import android.view.View;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public interface RecyclerViewListener {
    void onClickListener(View view, int position);
}