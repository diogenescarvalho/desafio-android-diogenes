package uol.diogenes.com.desafioandroid.adapters;

import uol.diogenes.com.desafioandroid.R;
import uol.diogenes.com.desafioandroid.models.Shot;
import uol.diogenes.com.desafioandroid.utilities.RecyclerViewListener;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class ShotsAdapter extends RecyclerView.Adapter<ShotsAdapter.ShotsViewHolder>{

    private RecyclerViewListener rvOnClickListener;
    private LayoutInflater layoutInflater;
    private List<Shot> shots;
    private Context context;

    /**
     *
     * inicializador dos componentes da Recycler View personalizada e
     * método para receber a posição do SHOT selecionado para que seja enviado
     * os dados parar a Activity DetalhesShotDribbleActivity
     *
     */
    public class ShotsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView ivShot;
        private TextView tvViews;
        private TextView tvCmentarios;
        private TextView tvLikes;
        private TextView tvAutor;

        public ShotsViewHolder(View itemView) {
            super(itemView);

            ivShot = (ImageView)itemView.findViewById(R.id.iv_card_shot);
            tvViews = (TextView)itemView.findViewById(R.id.tv_views);
            tvCmentarios = (TextView)itemView.findViewById(R.id.tv_comentarios);
            tvLikes = (TextView)itemView.findViewById(R.id.tv_likes);
            tvAutor = (TextView)itemView.findViewById(R.id.tv_autor_shot_selecionado);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(rvOnClickListener != null){
                rvOnClickListener.onClickListener(view, getAdapterPosition());
            }
        }
    }

    public void setRecyclerViewOnClickListener(RecyclerViewListener recyclerview){
        rvOnClickListener = recyclerview;
    }

    public ShotsAdapter(List<Shot> shots, Context context){
        this.shots = shots;
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ShotsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_shot_cardview, parent, false);
        ShotsViewHolder myViewHolder = new ShotsViewHolder(view);
        return myViewHolder;
    }

    //Seta dados nos componentes visuais do adapter
    @Override
    public void onBindViewHolder(ShotsViewHolder shotHolder, int position) {

        setShotImage(shots.get(position).getImagens().getHidpi(), shotHolder.ivShot);
        shotHolder.tvViews.setText(Integer.toString(shots.get(position).getViewsCount()));
        shotHolder.tvCmentarios.setText(Integer.toString(shots.get(position).getCommentsCount()));
        shotHolder.tvLikes.setText(Integer.toString(shots.get(position).getLikesCount()));
        shotHolder.tvAutor.setText(shots.get(position).getUser().getNome());

    }

    @Override
    public int getItemCount() {
        return shots.size();
    }

    //Seta a imagem já com o tamanho configurado
    private void setShotImage(String imageUrl, ImageView ivShot){
        Picasso.with(context)
                .load(imageUrl)
                .placeholder(R.drawable.dribbble_icon)
                .error(R.drawable.dribbble_icon)
                .resize(800, 600)
                .into(ivShot);
    }


}