package uol.diogenes.com.desafioandroid.activities;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

import uol.diogenes.com.desafioandroid.BaseActivity;
import uol.diogenes.com.desafioandroid.models.Shot;
import uol.diogenes.com.desafioandroid.R;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class DetalhesShotDribbleActivity extends BaseActivity {

    private Shot shotModel;

    @BindView(R.id.iv_shot_selecionado)
    protected ImageView ivBannerShot;
    @BindView(R.id.tv_autor_shot_selecionado)
    protected TextView tvAuthor;
    @BindView(R.id.tv_views_shot_selecionado)
    protected TextView tvViews;
    @BindView(R.id.tv_comentarios_shot_selecionado)
    protected TextView tvComments;
    @BindView(R.id.tv_likes_shot_selecionado)
    protected TextView tvLikes;
    @BindView(R.id.tvDescricao)
    protected TextView tvDescricao;
    @BindView(R.id.facebook)
    protected ImageView ivFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_shot_dribbble);
        ButterKnife.bind(this);
        initView();
    }

    //Inicialiação da VIEW com os detalhes do item selecinado da HomeListDribbbleActivity
    private void initView(){
        shotModel = (Shot) getIntent().getSerializableExtra(SELECIONADO);
        setShotBanner(shotModel.getImagens().getHidpi());
        setShotInfos();
    }

    //Insere a imagem destacada do item selecionado na HomeListDribbbleActivity
    private void setShotBanner(String imageUrl){
        Picasso.with(this)
                .load(imageUrl)
                .resize(1200,800)
                .into(ivBannerShot);
    }

    //Insere os dados do item selecionado na HomeListDribbbleActivity
    private void setShotInfos(){
        tvAuthor.setText(shotModel.getUser().getNome());
        tvViews.setText(Integer.toString(shotModel.getViewsCount()));
        tvComments.setText(Integer.toString(shotModel.getCommentsCount()));
        tvLikes.setText(Integer.toString(shotModel.getLikesCount()));

        /**
         * Necessário condicional para verificar o SDK >= 24 para apresentar
         * a descrição que está no formato HTML com uma saída amigável para o usuário
         *
         * Exemplo retornado pela API = "<p>Quick, messy, five minute sketch of something that might become a fictional something.</p>"
         */
        if (Build.VERSION.SDK_INT >= 24) {
            tvDescricao.setText(Html.fromHtml(shotModel.getDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvDescricao.setText(Html.fromHtml(shotModel.getDescription()));
        }
        ivFacebook.setOnClickListener(facebookShotSharing());
    }

    //Método para compartilhar o item selcionado no Facebook
    private View.OnClickListener facebookShotSharing(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareDialog facebookShareDialog = new ShareDialog(DetalhesShotDribbleActivity.this);

                if(ShareDialog.canShow(ShareLinkContent.class)){
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(shotModel.getHtmlUrl()))
                            .build();
                    facebookShareDialog.show(linkContent);
                }
            }
        };
    }
}
