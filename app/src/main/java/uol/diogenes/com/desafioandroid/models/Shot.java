package uol.diogenes.com.desafioandroid.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class Shot implements Serializable{

    /**
     * Na API contém diversos outros itens que poderiam estar na MODEL.
     * Porém estou implementando na Model somente o que é necessário para o desenvolvimento deste teste
     */
    @SerializedName("id")
    private int id;
    @SerializedName("description")
    private String description;
    @SerializedName("width")
    private int width;
    @SerializedName("height")
    private int height;
    @SerializedName("images")
    private Imagens imagens;
    @SerializedName("views_count")
    private int viewsCount;
    @SerializedName("likes_count")
    private int likesCount;
    @SerializedName("comments_count")
    private int commentsCount;
    @SerializedName("html_url")
    private String htmlUrl;
    @SerializedName("user")
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Imagens getImagens() {
        return imagens;
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public User getUser() {
        return user;
    }

}