package uol.diogenes.com.desafioandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import uol.diogenes.com.desafioandroid.R;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
//Inicializado do Splash do aplicativo
public class SplashScreenActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_dribbble);

        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                abreHomeListaDribbbleActiity();
            }
        }, 3500);
    }

    private void abreHomeListaDribbbleActiity() {
        Intent intent = new Intent(SplashScreenActivity.this,
                HomeListDribbbleActivity.class);
        startActivity(intent);
        finish();
    }

}
