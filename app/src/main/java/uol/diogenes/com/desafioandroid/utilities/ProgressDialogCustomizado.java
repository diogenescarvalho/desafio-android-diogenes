package uol.diogenes.com.desafioandroid.utilities;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
import uol.diogenes.com.desafioandroid.R;

public class ProgressDialogCustomizado {

    private static ProgressDialog progressDialog;

    public static void start(Context context){
        progressDialog = new ProgressDialog(context, R.style.TransparentProgressDialogTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    public static void stop(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

}