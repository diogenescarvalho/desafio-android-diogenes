package uol.diogenes.com.desafioandroid.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class DribbbleBuilder {

    private Retrofit retrofit;
    private DribbbleService service;

    public DribbbleBuilder(String baseURL){
        retrofit = initRetrofit(baseURL);
        service = initService(retrofit);
    }

    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private Retrofit initRetrofit(String baseURL){
        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private DribbbleService initService(Retrofit retrofit){
        return retrofit.create(DribbbleService.class);
    }

    public DribbbleService getService(){
        return this.service;
    }

}