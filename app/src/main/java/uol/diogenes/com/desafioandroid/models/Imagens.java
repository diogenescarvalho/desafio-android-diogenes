package uol.diogenes.com.desafioandroid.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class Imagens implements Serializable{

    /**
     * Na API contém diversos outros itens que poderiam estar na MODEL.
     * Porém estou implementando na Model somente o que é necessário para o desenvolvimento deste teste
     */
    //Tipo de imagens
    @SerializedName("hidpi")
    private String hidpi;

    public String getHidpi() {
        return hidpi;
    }



}
