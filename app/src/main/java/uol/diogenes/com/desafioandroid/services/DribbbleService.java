package uol.diogenes.com.desafioandroid.services;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

import uol.diogenes.com.desafioandroid.models.Shot;


/**
 * Created by diogenescarvalho on 22/09/17.
 */
public interface DribbbleService {

    //Carregar shots
    @GET("shots")
    @Headers("Authorization: Bearer df3ba111c99bdf4bc74decca940365fad44aecb5bfd08a296c618df15cb6590b")
    Call<List<Shot>> getShots(@Query("page") int page,
                              @Query("sort") String sort);

}



