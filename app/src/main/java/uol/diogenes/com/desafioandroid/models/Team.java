package uol.diogenes.com.desafioandroid.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class Team implements Serializable{

    /**
     * Na API contém diversos outros itens que poderiam estar na MODEL.
     * Porém estou implementando na Model somente o que é necessário para o desenvolvimento deste teste
     */
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("username")
    private String userName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}