package uol.diogenes.com.desafioandroid;

import android.content.Context;
import android.content.SharedPreferences;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class BaseApplication extends android.app.Application {

    private static final String PREFERENCIA_FILTRO = "BaseApplication.PREFERENCIA_FILTRO";
    /** Na 1º instalação a lista é renderizada com os itens populares.
     * Sempre que for alterando o tipo de filtro,
     * o último selecionado até o app ficar em onPause ou ser Fechado é salvo o Shared Preferences para a próxima seção
     */
    private static final String ULTIMO_FILTRO_SELECIONADO = "popular";
    private static Context instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        buildPicassoImageCache();
    }

    /**Builder do component para download e cache de imagens
     * http://square.github.io/picasso/
     */
    private void buildPicassoImageCache(){
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }

    //Instancia do Shared Preferences
    private static SharedPreferences getPreferenciaFiltro(){
        return instance.getSharedPreferences(PREFERENCIA_FILTRO, MODE_PRIVATE);
    }

    /**
     * Renderiza a lista com o último filtro selecionado pelo usuário
     */
    public static String ultimoFiltroSelecionado(){
        String ultimo = getPreferenciaFiltro().getString(PREFERENCIA_FILTRO, null);
        return ultimo != null ? ultimo : ULTIMO_FILTRO_SELECIONADO;
    }

    /**
     * Renderiza a lista com o último filtro selecionado pelo usuário
     */
    public static void setPreferenciaFiltro(String sortOption){
        SharedPreferences preferences = instance.getSharedPreferences(PREFERENCIA_FILTRO, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREFERENCIA_FILTRO, sortOption);
        editor.apply();
    }

}