package uol.diogenes.com.desafioandroid.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class User implements Serializable {

    /**
     * Na API contém diversos outros itens que poderiam estar na MODEL.
     * Porém estou implementando somente o que é necessário para o desenvolvimento deste teste.
     */
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String nome;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

}