package uol.diogenes.com.desafioandroid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import uol.diogenes.com.desafioandroid.services.DribbbleBuilder;
import uol.diogenes.com.desafioandroid.services.DribbbleService;
import uol.diogenes.com.desafioandroid.utilities.ProgressDialogCustomizado;


/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class BaseActivity extends AppCompatActivity {

    private static String dribbbleURL = "https://api.dribbble.com/v1/";
    protected final static String SELECIONADO = "SELECIONADO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void showProgressDialog(){
        ProgressDialogCustomizado.start(this);
    }

    protected void stopProgressDialog(){
        ProgressDialogCustomizado.stop();
    }

    protected DribbbleService getShotsService(){
        DribbbleBuilder builder = new DribbbleBuilder(dribbbleURL);
        return builder.getService();
    }

    protected String getSortPreference(){
        return BaseApplication.ultimoFiltroSelecionado();
    }

    protected void setSortPreference(String sortOption){
        BaseApplication.setPreferenciaFiltro(sortOption);
    }
}
