package uol.diogenes.com.desafioandroid.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import uol.diogenes.com.desafioandroid.BaseActivity;
import uol.diogenes.com.desafioandroid.PermissoesTeste;
import uol.diogenes.com.desafioandroid.R;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uol.diogenes.com.desafioandroid.adapters.ShotsAdapter;
import uol.diogenes.com.desafioandroid.models.Shot;
import uol.diogenes.com.desafioandroid.utilities.RecyclerViewListener;

/**
 * Created by diogenescarvalho on 22/09/17.
 */
public class HomeListDribbbleActivity extends BaseActivity implements RecyclerViewListener {

    private ShotsAdapter shotsAdapter;
    private List<Shot> shots;

    private int page;
    private String sort;
    private int ultimaPaginaScroll;

    @BindView(R.id.rv_shots_lista)
    protected RecyclerView rvShots;

    private static final String TAG = "UOL";
    private final static String POPULARES = "popular";
    private final static String RECENTES = "recent";

    /**
     * Uilizado um facilitador para utilização do Swipe and Pull to refresh
     * https://github.com/omadahealth/SwipyRefreshLayout
     */
    @BindView(R.id.swipe_pull_shots)
    protected SwipyRefreshLayout swipeShots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_list_shot_dribbble);
        ButterKnife.bind(this);

        initView();
        getShots(page, sort);

        //Exemplo para o teste de permissão em tempo de execução
        PermissoesTeste testeUolPermissao = new PermissoesTeste(HomeListDribbbleActivity.this);
        if (!testeUolPermissao.checarPermissaoMemoriaExterna()) {
            testeUolPermissao.permissaoMemoriaExterna(PermissoesTeste.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE);
        }
    }

    /**
     *
     * Exemplo para o teste de permissão em tempo de execução
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PermissoesTeste.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this.getApplicationContext(), "O aplicativo necessita da permissão.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    //Inicialiação da VIEW com o RecyclerView renderizado conforme o último filtro selecionado
    private void initView(){
        initRecyclerView();
        setDefaultShotsAttributes();
        swipeShots.setOnRefreshListener(swipeListener());
    }

    //Retrofit - GET dos itens da API do Dribbble
    private void getShots(final int pagina, String sort){

        if(pagina == 1)
            showProgressDialog();

        if(pagina < ultimaPaginaScroll) {
            getShotsService().getShots(pagina, sort).enqueue(new Callback<List<Shot>>() {
                @Override
                public void onResponse(Call<List<Shot>> call, Response<List<Shot>> response) {
                    if(response.body() != null && !response.body().isEmpty()) {
                        int shotsSize = shots.size();
                        shots.addAll(response.body());
                        recarregarSwipeShots();
                        setScrollPosition(shotsSize);
                        stopProgressDialog();
                    }else{
                        ultimaPaginaScroll = pagina;
                        swipeShots.setRefreshing(false);
                        swipeShots.setEnabled(false);
                    }
                }
                @Override
                public void onFailure(Call<List<Shot>> call, Throwable t) {
                    stopProgressDialog();
                    Toast.makeText(HomeListDribbbleActivity.this, R.string.erro_ao_listar_shots, Toast.LENGTH_LONG).show();
                    Log.i(TAG, "Erro na resposta"+t.getMessage());
                }
            });
        }
    }

    /**
     * Inicializa o Recycle View
     */
    private void initRecyclerView(){
        shots = new ArrayList<>();
        shotsAdapter = new ShotsAdapter(shots, this);
        shotsAdapter.setRecyclerViewOnClickListener(this);
        rvShots.setAdapter(shotsAdapter);
    }

    /**
     * Recarrega mais SHOTS com o swipe
     */
    private void recarregarSwipeShots(){
        shotsAdapter.notifyDataSetChanged();
        swipeShots.setRefreshing(false);
    }

    private void resetShotsListConfig(String newSort){
        shots.clear();
        sort = newSort;
        swipeShots.setEnabled(true);
        ultimaPaginaScroll = Integer.MAX_VALUE;
        getShots(page, sort);
    }

    /**
     * Swipe Refresh com mais SHOTS
     */
    private SwipyRefreshLayout.OnRefreshListener swipeListener(){
        return new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                page++;
                getShots(page, sort);
            }
        };
    }

    //Scroll até a posição do último item antes do Swipe
    private void setScrollPosition(int shotSize){
        LinearLayoutManager layoutManager = (LinearLayoutManager)rvShots.getLayoutManager();
        layoutManager.scrollToPosition(shotSize + 1);
        rvShots.setLayoutManager(layoutManager);
    }

    private void setDefaultShotsAttributes(){
        sort = getSortPreference();
        ultimaPaginaScroll = Integer.MAX_VALUE;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Opções do menu para filtragem da lista
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.filtro_popular:
                resetShotsListConfig(POPULARES);
                setSortPreference(POPULARES);
                return true;
            case R.id.filtro_recente:
                resetShotsListConfig(RECENTES);
                setSortPreference(RECENTES);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Envio do item selecionado para a Activity de Detalhes.
     * @param view
     * @param position
     */
    @Override
    public void onClickListener(View view, int position) {
        Intent intent = new Intent(HomeListDribbbleActivity.this, DetalhesShotDribbleActivity.class);
        intent.putExtra(SELECIONADO, shots.get(position));
        startActivity(intent);
    }
}